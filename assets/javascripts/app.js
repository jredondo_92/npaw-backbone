MyApp = new Backbone.Marionette.Application();
 
MyApp.addRegions({
  mainRegion: "#content"
});

Album = Backbone.Model.extend({});

Albums = Backbone.Collection.extend({
    model: Album
})

AlbumView = Backbone.Marionette.ItemView.extend({
    template: "#album-template",
    tagName: 'p',
    className: 'album',
})

AlbumsView = Backbone.Marionette.CompositeView.extend({
    id: "albums",
    className: "album-list",
    template: "#albums-template",
    itemView: AlbumView,

    initialize: function(){
        this.listenTo(this.collection, "sort", this.renderCollection);
    },
   
    appendHtml: function(collectionView, itemView){
        collectionView.$(".album-container").append(itemView.el);
    }
});

MyApp.addInitializer(function(options){
    var albumsView = new AlbumsView({
        collection: options.albums
    });
    MyApp.mainRegion.show(albumsView);
});

$(document).ready(function(){
var albums = new Albums([
    new Album({ name: 'Wet Cat', image_path: 'assets/images/cat2.jpg' }),
    new Album({ name: 'Bitey Cat', image_path: 'assets/images/cat1.jpg' }),
    new Album({ name: 'Surprised Cat', image_path: 'assets/images/cat3.jpg' })
]);

    MyApp.start({albums: albums});
});